let controller = {
    getAll: (req, res, next) => {res.end(`Will send all the books to you !`);},
    addOne: (req, res, next) => {res.end(`Will add the book : "${req.body.name}" with details : "${req.body.description}"`);},
    deleteAll: (req, res, next) => {res.end(`Will delete all the books for you !`);},
    getOne: (req, res, next) => {res.end(`Will send the details of the book ${req.params.id} to you !`);},
    updateOne: (req, res, next) => {res.end(`Will update book ${req.params.id} with description : "${req.body.description}"`);},
    deleteOne: (req, res, next) => {res.end(`Will delete book ${req.params.id} for you !`);}
};


module.exports = controller;