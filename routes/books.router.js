
const express = require('express');

const controller = require('../controller/books.controller');
let router = express.Router();

router.all("/", (req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/html");
    next();
});
router.get("/", (req, res, next) => {console.log('Hi'); next();}, controller.getAll);
router.post("/", controller.addOne);
router.delete("/", controller.deleteAll);

router.get("/:id", controller.getOne);
router.put("/:id", controller.updateOne);
router.delete("/:id", controller.deleteOne);

module.exports = router;
