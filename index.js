const express = require("express");
const morgan = require("morgan");
const http = require("http");

const booksRouter = require("./routes/books.router");

const hostname = "localhost";
const port = 3000;

const app = express();

app.use(morgan("dev"));

app.use(express.static(__dirname + "/public"));

app.use(express.json());

app.use("/books", booksRouter);

app.use((req, res, next) => {
    //console.log(req.headers);
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/html");
    res.end("<html><body><h1>Hello express-morgan !</h1></body></html>");
})

const server = http.createServer(app);

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}`);
})